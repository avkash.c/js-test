var assert = require("assert");

var voters1 = [
  { name: "Bob", age: 30, voted: true },
  { name: "Jake", age: 32, voted: true },
  { name: "Kate", age: 25, voted: false },
  { name: "Sam", age: 20, voted: false },
  { name: "Phil", age: 21, voted: true },
  { name: "Ed", age: 55, voted: true },
  { name: "Tami", age: 54, voted: true },
  { name: "Mary", age: 31, voted: false },
  { name: "Becky", age: 43, voted: false },
  { name: "Joey", age: 41, voted: true },
  { name: "Jeff", age: 30, voted: true },
  { name: "Zack", age: 19, voted: false }
];

var voters2 = [
  { name: "Avkash", age: 18, voted: true },
  { name: "Parth", age: 20, voted: false },
  { name: "Jayvir", age: 34, voted: false },
  { name: "Mahima", age: 37, voted: false },
  { name: "Parita", age: 26, voted: false },
  { name: "Dhyey", age: 48, voted: true }
];

function voterResults(arr) {
  // your code here
  var voteDetails = {};

  var youth = arr.filter(obj => obj.age >= 18 && obj.age <= 25);
  voteDetails.youngVotes = youth.filter(obj => obj.voted == true).length;
  voteDetails.youth = youth.length;

  var mid = arr.filter(obj => obj.age >= 26 && obj.age <= 35);
  voteDetails.midVotes = mid.filter(obj => obj.voted == true).length;
  voteDetails.mids = mid.length;

  var old = arr.filter(obj => obj.age >= 36 && obj.age <= 55);
  voteDetails.oldVotes = old.filter(obj => obj.voted == true).length;
  voteDetails.olds = old.length;

  return voteDetails;
}

assert.deepEqual(voterResults(voters1), {
  youngVotes: 1,
  youth: 4,
  midVotes: 3,
  mids: 4,
  oldVotes: 3,
  olds: 4
});

assert.deepEqual(voterResults(voters2), {
  youngVotes: 1,
  youth: 2,
  midVotes: 0,
  mids: 2,
  oldVotes: 1,
  olds: 2
});

// console.log(voterResults(voters1));
// console.log(voterResults(voters2)); // Returned value shown below:
/*
{ 
 youngVotes: 1,
 youth: 4,
 midVotes: 3,
 mids: 4,
 oldVotes: 3,
 olds: 4
}
*/
// Include how many of the potential voters were in the ages 18-25,
// how many from 26-35, how many from 36-55, and how many of each of those age ranges actually voted.
// The resulting object containing this data should have 6 properties.
// See the example output at the bottom.
