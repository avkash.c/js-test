// Create a doubly linked list.
// Write functions for inserting and iterating in linked list.
// Use js various structures to implement the same

class Node {
  constructor(value) {
    this.value = value;
    this.next = null;
    this.prev = null;
  }
}

class Linklist {
  constructor() {
    this.head = null;
    this.last = null;
  }

  insert(num) {
    if (this.head == null && this.last == null) {
      this.head = new Node(num);
      this.last = this.head;
    } else {
      this.last.next = new Node(num);
      this.last.next.prev = this.last;
      this.last = this.last.next;
    }
  }
}

var l = new Linklist();
l.insert(123);
l.insert(456);
l.insert(789);
console.log(l);
