var arr1 = [
  "0e089d7ee981bec654acb4a678cef7e6",
  "7adefed65ed36cb257a5e44bc0ad9919",
  "64450e7b290f8abcfb070a27d5eaf202"
];
var arr2 = [
  {
    hashKey: "7adefed65ed36cb257a5e44bc0ad9919",
    updatedAt: new Date("2020-02-02T07:56:44.731Z"),
    sizeInBytes: 127543,
    storePath: "arxiv/pdf/astro-ph0001011.pdf",
    createdAt: new Date("2020-02-02T07:56:44.731Z")
  },
  {
    hashKey: "0e089d7ee981bec654acb4a678cef7e6",
    updatedAt: new Date("2020-02-07T07:56:44.731Z"),
    sizeInBytes: 197563,
    storePath: "arxiv/pdf/astro-ph0001018.pdf",
    createdAt: new Date("2020-02-07T07:56:44.731Z")
  },
  {},
  {
    updatedAt: new Date("2020-02-06T07:56:44.731Z"),
    sizeInBytes: 15705,
    storePath: "arxiv/pdf/astro-ph0001010.pdf",
    createdAt: new Date("2020-02-06T07:56:44.731Z")
  }
];
// There is an array (arr1) containing hash value and a second array (arr2) contains array of objects.
// Sort arr2 in the same order as arr1 using "hashKey". Come up with an optimized solution.

function sortArrObj(arr1, arr2) {
  var sorted = arr1.map(arrObj1 => {
    var newObj = arr2.filter(arrObj2 => arrObj1 == arrObj2.hashKey);
    if (newObj[0] == undefined) {
      return;
    }
    return newObj[0];
  });
  arr2
    .filter(obj => obj.hashKey == undefined)
    .map(obj => {
      sorted.push(obj);
    });

  //   console.log(arr2.filter(obj => obj.hashKey == undefined));
  return sorted;
}

console.log(sortArrObj(arr1, arr2));
