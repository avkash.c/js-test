// Create a function which return an array by formula i = p x r x t ÷ 100.
// Use map, filter functions
// Remove negative numbers from array
// Answer is in the format of an array of integer round up to 2 decimal places. Eg. [1.01, 2.23]
// Create unit test using assertion.

var assert = require("assert");

const test1 = [
  {
    p: 73,
    r: 98,
    t: 11
  },
  {
    p: 25,
    r: 37,
    t: 77
  },
  {
    p: 34,
    r: -44,
    t: 69
  },
  {
    p: 86,
    r: 25,
    t: 27
  },
  {
    p: 27,
    r: -38,
    t: 11
  },
  {
    p: 4,
    r: 11,
    t: 66
  },
  {
    p: 10,
    r: 10,
    t: 20
  }
];

const test2 = [
  {
    p: -43,
    r: 98,
    t: -1
  },
  {
    p: 25,
    r: 37,
    t: -77
  },
  {
    p: 34,
    r: 44,
    t: 69
  },
  {
    p: 86,
    r: -25,
    t: -27
  },
  {
    p: 27,
    r: -38,
    t: 11
  }
];

const test3 = [
  {
    p: -73,
    r: -98,
    t: -11
  },
  {
    p: 25,
    r: -37,
    t: -77
  },
  {
    p: 10,
    r: 10,
    t: 20
  }
];

function interest(test) {
  var Interest = test.map(obj => (obj.p * obj.r * obj.t) / 100);
  answer = Interest.filter(obj => obj >= 0);
  return answer;
}

// console.log(interest(test3));
assert.deepEqual(interest(test1), [786.94, 712.25, 580.5, 29.04, 20.0]);
assert.deepEqual(interest(test2), [42.14, 1032.24, 580.5]);
assert.deepEqual(interest(test3), [712.25, 20]);
